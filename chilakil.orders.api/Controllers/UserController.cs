using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace chilakil.orders.api.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        [ApiExplorerSettings(IgnoreApi = true)]
        public int GetUserId()
        {
            return int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public string GetUserPhone()
        {
            return User.FindFirst(ClaimTypes.HomePhone).Value;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public string GetUserAccountKitId()
        {
            return User.FindFirst(ClaimTypes.UserData).Value;
        }
    }
}