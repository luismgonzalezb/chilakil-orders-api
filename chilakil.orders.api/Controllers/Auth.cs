﻿using System;
using chilakil.orders.business.Dtos;
using chilakil.orders.infrastructure.Auth;
using Microsoft.AspNetCore.Mvc;

namespace chilakil.orders.api.Controllers
{
    [Route("api/v1/auth")]
    public class Auth : Controller
    {
        private readonly IJwtFactory _jwtFactory;

        public Auth(IJwtFactory jwtFactory)
        {
            _jwtFactory = jwtFactory;
        }

        [HttpPost]
        public IActionResult RequestToken([FromBody] TokenRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var apiKey = Environment.GetEnvironmentVariable("CHILAKIL_API_KEY");
            if (request.apiKey != apiKey) return Unauthorized();

            var token = _jwtFactory.GenerateEncodedToken(request.userKey);

            if (token == null) return Unauthorized();

            return Ok(new {token});
        }
    }
}