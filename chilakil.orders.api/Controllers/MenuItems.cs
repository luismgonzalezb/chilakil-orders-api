﻿using System.Collections.Generic;
using chilakil.orders.business.Contracts;
using chilakil.orders.business.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace chilakil.orders.api.Controllers
{
    [Route("api/v1/menuitems")]
    public class MenuItems : Controller
    {
        private readonly IMenuItemService _service;

        public MenuItems(IMenuItemService service)
        {
            _service = service;
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<MenuItemDto>))]
        public IActionResult Index()
        {
            return new ObjectResult(_service.GetAll());
        }
    }
}