using System.Collections.Generic;
using chilakil.orders.business.Contracts;
using chilakil.orders.business.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace chilakil.orders.api.Controllers.Admin
{
    [Route("api/v1/admin/restaurants")]
    public class Restaurants : UserController
    {
        private readonly IRestaurantService _service;

        public Restaurants(IRestaurantService service)
        {
            _service = service;
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<RestaurantDto>))]
        public IActionResult Index()
        {
            var id = GetUserId();
            var restaurants = _service.GetUserRestaurants(id);
            return new ObjectResult(restaurants);
        }

        [HttpPost]
        [Produces("application/json", Type = typeof(RestaurantDto))]
        public IActionResult AddRestaurant([FromBody] RestaurantDto restaurantDto)
        {
            var id = GetUserId();
            var restaurant = _service.AddRestaurant(restaurantDto.Name, restaurantDto.Description, id);
            return new ObjectResult(restaurant);
        }

        [HttpDelete("{restaurantId}")]
        [Produces("application/json", Type = typeof(RestaurantDto))]
        public IActionResult DeleteRestaurant([FromRoute] int restaurantId)
        {
            var id = GetUserId();
            var deleted = _service.DeleteRestaurant(id, restaurantId);
            if (deleted) return new OkResult();
            return new UnauthorizedResult();
        }
    }
}