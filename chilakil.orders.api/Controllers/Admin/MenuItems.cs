﻿using chilakil.orders.business.Contracts;
using chilakil.orders.business.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace chilakil.orders.api.Controllers.Admin
{
    [Authorize(Roles = "ADMIN")]
    [Route("api/v1/admin/menuitems")]
    public class MenuItems : UserController
    {
        private readonly IMenuItemService _service;

        public MenuItems(IMenuItemService service)
        {
            _service = service;
        }

        [HttpPost]
        [Produces("application/json", Type = typeof(MenuItemDto))]
        public IActionResult PostMenuItem([FromBody] MenuItemDto menuItem)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var newMenuItem = _service.Add(menuItem);
            return Created("PostMenuItem", newMenuItem);
        }
    }
}