﻿using chilakil.orders.business.Contracts;
using chilakil.orders.business.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace chilakil.orders.api.Controllers.Admin
{
    [Route("api/v1/admin/users")]
    public class UsersController : UserController
    {
        private readonly IUsersService _service;

        public UsersController(IUsersService service)
        {
            _service = service;
        }

        [AllowAnonymous]
        [HttpPost("fb")]
        [Produces("application/json", Type = typeof(UsersDto))]
        public IActionResult CreateOrGetUserByAccountkitId([FromBody] UsersSecurityDto baseUser)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = _service.GetUserOrCreateByAccountId(baseUser.AccountKitId, baseUser.Phone);
            if (user == null) return NotFound();
            return new ObjectResult(user);
        }

        [HttpPut]
        [Produces("application/json", Type = typeof(UsersDto))]
        public IActionResult Update([FromBody] UsersDto user)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var id = GetUserId();
            var phone = GetUserPhone();
            var accountKitId = GetUserAccountKitId();
            var updatedUSer = _service.Update(id, accountKitId, phone, user);
            return new ObjectResult(updatedUSer);
        }

        [HttpGet]
        [Produces("application/json", Type = typeof(UsersDto))]
        public IActionResult GetUser()
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var id = GetUserId();
            var phone = GetUserPhone();
            var accountKitId = GetUserAccountKitId();
            var user = _service.GetUserByAuth(id, accountKitId, phone);
            if (user == null) return NotFound();

            return new ObjectResult(user);
        }
    }
}