using System;
using System.Text;
using chilakil.orders.infrastructure.Auth;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace chilakil.orders.api.Middleware
{
    public class ServicesAuthenticationStartup
    {
        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IJwtFactory, JwtFactory>();

            var jwtIssuerOptions = new JwtIssuerOptions();
            configuration.Bind("JwtIssuerOptions", jwtIssuerOptions);
            services.AddAuthentication(SetAuthenticationOptions)
                .AddJwtBearer(options => SetJwtBearerOptions(options, jwtIssuerOptions));
        }

        private static string GetTokenKey()
        {
            return Environment.GetEnvironmentVariable("CHILAKIL_COOKIE_KEY");
        }

        private static void SetAuthenticationOptions(AuthenticationOptions options)
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }

        private static void SetJwtBearerOptions(JwtBearerOptions options, IJwtIssuerOptions jwtIssuerOptions)
        {
            var tokenKey = GetTokenKey();
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = jwtIssuerOptions.Issuer,
                ValidAudience = jwtIssuerOptions.Audience,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenKey))
            };
        }
    }
}