using System;
using chilakil.orders.business.Contracts;
using chilakil.orders.business.Services;
using chilakil.orders.infrastructure.Data.Contracts;
using chilakil.orders.infrastructure.Data.Models;
using chilakil.orders.infrastructure.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace chilakil.orders.api.Middleware
{
    public class ServicesDataStartup
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IMenuItemRepository, MenuItemRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IRestaurantRepository, RestaurantRepository>();
            services.AddScoped<IMenuItemService, MenuItemService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IRestaurantService, RestaurantService>();

            var connectionString = Environment.GetEnvironmentVariable("CHILAKIL_DB_STR");
            if (connectionString != null)
                services.AddDbContext<ChilakilContext>(options => options.UseMySql(connectionString));
        }
    }
}