using chilakil.orders.business.Contracts;
using chilakil.orders.business.Services;
using chilakil.orders.test.Fixtures;
using Xunit;

namespace chilakil.orders.test.business.Services
{
    [Collection("WithDatabase")]
    public class UsersServiceTests : IClassFixture<MapperFixture>
    {
        public UsersServiceTests(RepositoriesFixture fixture, MapperFixture mapperFixture)
        {
            _service = new UsersService(fixture.UsersRepository, mapperFixture.mapper);
        }

        private IUsersService _service { get; }

        [Fact]
        public void Test_GetUserOrCreateByAccountId_OK()
        {
            var user = _service.GetUserOrCreateByAccountId(ConstantsFixture.AccountKitId, ConstantsFixture.Phone);
            Assert.True(user.UserId > 0);
        }
    }
}