using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using chilakil.orders.business.Dtos;
using chilakil.orders.infrastructure.Crypto;
using Newtonsoft.Json.Linq;

namespace chilakil.orders.test.Fixtures
{
    public class UserControllerHelper
    {
        private readonly HttpClient _client;

        public UserControllerHelper(HttpClient client)
        {
            _client = client;
        }

        public async Task<UsersSecurityDto> GetUserOrCreateByAccountId()
        {
            var user = new UsersSecurityDto
            {
                Phone = ConstantsFixture.Phone,
                AccountKitId = ConstantsFixture.AccountKitId
            };
            var httpResponse = await _client.PostAsJsonAsync("api/v1/admin/users/fb", user);
            user = await httpResponse.Content.ReadAsAsync<UsersSecurityDto>();
            return user;
        }

        public async Task<string> GetJwtToken(string apiKey)
        {
            var user = await GetUserOrCreateByAccountId();

            var text = new StringBuilder();
            text.Append(user.UserId);
            text.Append(":");
            text.Append(user.AccountKitId);
            text.Append(":");
            text.Append(user.Phone);
            var userKey = Decode.EncryptString(text.ToString(), apiKey);

            var tokenRequest = new TokenRequest
            {
                apiKey = apiKey,
                userKey = userKey
            };
            var httpResponse = await _client.PostAsJsonAsync("api/v1/auth", tokenRequest);
            var tokenObject = await httpResponse.Content.ReadAsAsync<JObject>();
            var token = tokenObject["token"];
            return token.ToString();
        }
    }
}