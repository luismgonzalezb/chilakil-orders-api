using chilakil.orders.infrastructure.Data.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace chilakil.orders.test.Fixtures
{
    public class CustomWebApplicationFactory<TStartup>
        : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                const string connectionString = "Server=localhost;Database=chilakil_test;User=root;Password=#Labs2013;";
                services.AddDbContext<ChilakilContext>(options => options.UseMySql(connectionString));
            });
        }
    }
}