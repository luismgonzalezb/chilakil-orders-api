using System;
using AutoMapper;
using chilakil.orders.business.Middleware;

namespace chilakil.orders.test.Fixtures
{
    public class MapperFixture : IDisposable
    {
        public MapperFixture()
        {
            var config = new MapperConfiguration(cfg => { cfg.AddProfile<MappingProfile>(); });
            mapper = new Mapper(config);
        }

        public IMapper mapper { get; }

        public void Dispose()
        {
        }
    }
}