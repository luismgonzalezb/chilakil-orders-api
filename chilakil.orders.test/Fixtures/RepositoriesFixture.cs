using System;
using chilakil.orders.infrastructure.Data.Contracts;
using chilakil.orders.infrastructure.Data.Models;
using chilakil.orders.infrastructure.Data.Repositories;
using Microsoft.EntityFrameworkCore;

namespace chilakil.orders.test.Fixtures
{
    public class RepositoriesFixture : IDisposable
    {
        public RepositoriesFixture()
        {
            const string connectionString = "Server=localhost;Database=chilakil_test;User=root;Password=#Labs2013;";
            var builder = new DbContextOptionsBuilder<ChilakilContext>();
            builder.UseMySql(connectionString);
            var options = builder.Options;
            ChilakilContext = new ChilakilContext(options);
            UsersRepository = new UsersRepository(ChilakilContext);
            RestaurantRepository = new RestaurantRepository(ChilakilContext);
        }

        public IUsersRepository UsersRepository { get; }
        public IRestaurantRepository RestaurantRepository { get; }
        private ChilakilContext ChilakilContext { get; }

        public void Dispose()
        {
            ChilakilContext.Dispose();
        }
    }
}