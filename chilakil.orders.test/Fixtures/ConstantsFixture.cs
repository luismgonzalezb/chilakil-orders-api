namespace chilakil.orders.test.Fixtures
{
    public static class ConstantsFixture
    {
        public const int UserId = 3;
        public const string AccountKitId = "1234567890";
        public const string Phone = "555-555-5555";
        public const string RestaurantName = "Automated Test";
        public const string RestaurantDescription = "Automated Test Description";
    }
}