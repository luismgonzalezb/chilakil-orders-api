using System;
using System.Net.Http;
using chilakil.orders.api;
using chilakil.orders.test.Fixtures;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace chilakil.orders.test.api.Controllers
{
    public class AdminUsersControllerTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        public AdminUsersControllerTest(WebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
            _helper = new UserControllerHelper(_client);
            _apiKey = Environment.GetEnvironmentVariable("CHILAKIL_API_KEY");
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        private readonly string _apiKey;
        private readonly HttpClient _client;
        private readonly UserControllerHelper _helper;

        [Fact]
        public async void Test_GetUserOrCreateByAccountId_OK()
        {
            var user = await _helper.GetUserOrCreateByAccountId();
            Assert.NotNull(user);
        }

        [Fact]
        public async void Test_RequestToken_OK()
        {
            var token = await _helper.GetJwtToken(_apiKey);
            Assert.NotNull(token);
        }
    }
}