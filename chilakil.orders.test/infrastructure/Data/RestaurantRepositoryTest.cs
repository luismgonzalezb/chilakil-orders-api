using chilakil.orders.test.Fixtures;
using Xunit;

namespace chilakil.orders.test.infrastructure.Data
{
    [Collection("WithDatabase")]
    public class RestaurantRepositoryTest
    {
        public RestaurantRepositoryTest(RepositoriesFixture fixture)
        {
            _fixture = fixture;
        }

        private readonly RepositoriesFixture _fixture;

        [Fact]
        public void Test_AddRestaurant_OK()
        {
            var restaurant = _fixture.RestaurantRepository.AddRestaurant(ConstantsFixture.RestaurantName,
                ConstantsFixture.RestaurantDescription, ConstantsFixture.UserId);
            Assert.NotNull(restaurant);
        }

        [Fact]
        public void Test_DeleteRestaurant_OK()
        {
            var restaurant = _fixture.RestaurantRepository.AddRestaurant(ConstantsFixture.RestaurantName,
                ConstantsFixture.RestaurantDescription, ConstantsFixture.UserId);
            var deleted =
                _fixture.RestaurantRepository.DeleteRestaurant(ConstantsFixture.UserId, restaurant.RestaurantId);
            Assert.True(deleted);
        }

        [Fact]
        public void Test_GetRestaurant_OK()
        {
            var restaurant = _fixture.RestaurantRepository.GetRestaurant(1);
            Assert.NotNull(restaurant);
        }

        [Fact]
        public void Test_GetUserRestaurants_OK()
        {
            var restaurant = _fixture.RestaurantRepository.GetUserRestaurants(ConstantsFixture.UserId);
            Assert.NotNull(restaurant);
        }
    }
}