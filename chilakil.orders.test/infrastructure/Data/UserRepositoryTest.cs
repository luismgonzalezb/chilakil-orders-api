using chilakil.orders.infrastructure.Data.Models;
using chilakil.orders.test.Fixtures;
using Xunit;

namespace chilakil.orders.test.infrastructure.Data
{
    [Collection("WithDatabase")]
    public class UserRepositoryTest
    {
        public UserRepositoryTest(RepositoriesFixture fixture)
        {
            _fixture = fixture;
        }

        private readonly RepositoriesFixture _fixture;

        [Fact]
        public void Test_GetUserByAuth_FAIL()
        {
            var byAuth = _fixture.UsersRepository.GetUserByAuth(ConstantsFixture.UserId, "abc", ConstantsFixture.Phone);
            Assert.Null(byAuth);
        }

        [Fact]
        public void Test_GetUserByAuth_OK()
        {
            var byAuth = _fixture.UsersRepository.GetUserByAuth(ConstantsFixture.UserId, ConstantsFixture.AccountKitId,
                ConstantsFixture.Phone);
            Assert.NotNull(byAuth);
        }

        [Fact]
        public void Test_GetUserOrCreateByAccountId_OK()
        {
            var user = _fixture.UsersRepository.GetUserOrCreateByAccountId(ConstantsFixture.AccountKitId,
                ConstantsFixture.Phone);
            Assert.True(user.UserId > 0);
        }

        [Fact]
        public void Test_UpdateUser_OK()
        {
            var user = new Users
            {
                Name = "Test",
                Lastname = "User",
                Email = "test@usermail.com"
            };
            var updated = _fixture.UsersRepository.Update(ConstantsFixture.UserId, ConstantsFixture.AccountKitId,
                ConstantsFixture.Phone, user);
            Assert.NotNull(updated);
            Assert.Equal("Test", updated.Name);
        }
    }
}