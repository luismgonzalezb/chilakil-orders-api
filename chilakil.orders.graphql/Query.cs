﻿namespace chilakil.orders.graphql
{
    public class Query
    {
        public Greetings GetGreetings()
        {
            return new Greetings();
        }
    }

    public class Greetings
    {
        public string Hello()
        {
            return "World";
        }
    }
}