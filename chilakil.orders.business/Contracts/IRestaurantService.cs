﻿using System.Collections.Generic;
using chilakil.orders.business.Dtos;

namespace chilakil.orders.business.Contracts
{
    public interface IRestaurantService
    {
        RestaurantDto GetRestaurant(int restaurantId);

        RestaurantDto AddRestaurant(string name, string description, int userId);

        IEnumerable<RestaurantDto> GetUserRestaurants(int userId);

        bool DeleteRestaurant(int userId, int restaurantId);
    }
}