﻿using System.Collections.Generic;
using chilakil.orders.business.Dtos;

namespace chilakil.orders.business.Contracts
{
    public interface IMenuItemService
    {
        IEnumerable<MenuItemDto> GetAll();

        MenuItemDto Add(MenuItemDto menuItem);
    }
}