using chilakil.orders.business.Dtos;

namespace chilakil.orders.business.Contracts
{
    public interface IUsersService
    {
        UsersDto Update(int id, string accountKitId, string phone, UsersDto user);

        UsersSecurityDto GetUserOrCreateByAccountId(string accountKitId, string phone);

        UsersSecurityDto GetUserByAuth(int id, string accountKitId, string phone);
    }
}