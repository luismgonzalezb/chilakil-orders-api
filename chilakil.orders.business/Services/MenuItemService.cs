﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using chilakil.orders.business.Contracts;
using chilakil.orders.business.Dtos;
using chilakil.orders.infrastructure.Data.Contracts;
using chilakil.orders.infrastructure.Data.Models;

namespace chilakil.orders.business.Services
{
    public class MenuItemService : IMenuItemService
    {
        private readonly IMapper _mapper;

        private readonly IMenuItemRepository _repository;

        public MenuItemService(IMenuItemRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<MenuItemDto> GetAll()
        {
            var result = _repository.GetAll();
            return result.Select(t => _mapper.Map<MenuItemDto>(t));
        }

        public MenuItemDto Add(MenuItemDto menuItem)
        {
            var item = _mapper.Map<MenuItemDto, MenuItem>(menuItem);
            var saved = _repository.Add(item);
            return _mapper.Map<MenuItemDto>(saved);
        }
    }
}