using AutoMapper;
using chilakil.orders.business.Contracts;
using chilakil.orders.business.Dtos;
using chilakil.orders.infrastructure.Data.Contracts;
using chilakil.orders.infrastructure.Data.Models;

namespace chilakil.orders.business.Services
{
    public class UsersService : IUsersService
    {
        private readonly IMapper _mapper;

        private readonly IUsersRepository _repository;

        public UsersService(IUsersRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public UsersDto Update(int id, string accountKitId, string phone, UsersDto user)
        {
            var _user = _mapper.Map<Users>(user);
            var result = _repository.Update(id, accountKitId, phone, _user);
            return _mapper.Map<UsersDto>(result);
        }

        public UsersSecurityDto GetUserOrCreateByAccountId(string accountKitId, string phone)
        {
            var result = _repository.GetUserOrCreateByAccountId(accountKitId, phone);
            return _mapper.Map<UsersSecurityDto>(result);
        }

        public UsersSecurityDto GetUserByAuth(int id, string accountKitId, string phone)
        {
            var result = _repository.GetUserByAuth(id, accountKitId, phone);
            return _mapper.Map<UsersSecurityDto>(result);
        }
    }
}