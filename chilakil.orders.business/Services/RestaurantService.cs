﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using chilakil.orders.business.Contracts;
using chilakil.orders.business.Dtos;
using chilakil.orders.infrastructure.Data.Contracts;

namespace chilakil.orders.business.Services
{
    public class RestaurantService : IRestaurantService
    {
        private readonly IMapper _mapper;

        private readonly IRestaurantRepository _repository;

        public RestaurantService(IRestaurantRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public RestaurantDto GetRestaurant(int restaurantId)
        {
            var result = _repository.GetRestaurant(1);
            return _mapper.Map<RestaurantDto>(result);
        }

        public RestaurantDto AddRestaurant(string name, string description, int userId)
        {
            var result = _repository.GetRestaurant(1);
            return _mapper.Map<RestaurantDto>(result);
        }

        public IEnumerable<RestaurantDto> GetUserRestaurants(int userId)
        {
            var result = _repository.GetUserRestaurants(1);
            return result.Select(t => _mapper.Map<RestaurantDto>(t));
        }

        public bool DeleteRestaurant(int userId, int restaurantId)
        {
            return true;
        }
    }
}