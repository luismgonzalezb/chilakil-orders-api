﻿namespace chilakil.orders.business.Dtos
{
    public class MenuIngredientsDto
    {
        public int MenuIngredientId { get; set; }
        public string Name { get; set; }
        public int? Alergenic { get; set; }
    }
}