﻿using System;

namespace chilakil.orders.business.Dtos
{
    public enum Days
    {
        M = 'M',
        T = 'T',
        W = 'W',
        R = 'R',
        F = 'F',
        S = 'S',
        U = 'U'
    }

    public class MenuServingTimesDto
    {
        public long MenuServingTimesId { get; set; }
        public int? MenuId { get; set; }
        public Days Day { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public MenusDto Menu { get; set; }
    }
}