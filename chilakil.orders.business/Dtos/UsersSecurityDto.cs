﻿namespace chilakil.orders.business.Dtos
{
    public class UsersSecurityDto : UsersDto
    {
        public int UserId { get; set; }
        public string Phone { get; set; }
        public string AccountKitId { get; set; }
    }
}