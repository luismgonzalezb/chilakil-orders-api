﻿using System.Collections.Generic;

namespace chilakil.orders.business.Dtos
{
    public class RestaurantDto
    {
        public int RestaurantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<LocationsDto> Locations { get; set; }
        public ICollection<UsersDto> Users { get; set; }
    }
}