﻿namespace chilakil.orders.business.Dtos
{
    public class RolesDto
    {
        public int RoleId { get; set; }
        public string Role { get; set; }
        public int Active { get; set; }
    }
}