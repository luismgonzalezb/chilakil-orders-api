﻿namespace chilakil.orders.business.Dtos
{
    public class MenuCategoryDto
    {
        public int MenuCategoryId { get; set; }
        public string Name { get; set; }
    }
}