﻿using System.Collections.Generic;

namespace chilakil.orders.business.Dtos
{
    public class MenusDto
    {
        public int MenuId { get; set; }
        public string Name { get; set; }

        public ICollection<LocationsDto> Locations { get; set; }
        public ICollection<MenuCategoryDto> MenuCategories { get; set; }
        public ICollection<MenuItemDto> MenuItems { get; set; }
        public ICollection<MenuServingTimesDto> MenuServingTimes { get; set; }
    }
}