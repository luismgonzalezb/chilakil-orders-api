﻿using System.Collections.Generic;

namespace chilakil.orders.business.Dtos
{
    public class UsersDto
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }

        public RolesDto Role { get; set; }
        public ICollection<RestaurantDto> Restaurants { get; set; }
    }
}