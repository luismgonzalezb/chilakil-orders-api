﻿using System.Collections.Generic;

namespace chilakil.orders.business.Dtos
{
    public class LocationsDto
    {
        public int LocationId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }

        public RestaurantDto Restaurant { get; set; }
        public ICollection<LocationConfigDto> LocationConfig { get; set; }
        public ICollection<LocationDeliveryDto> LocationDelivery { get; set; }
        public ICollection<MenusDto> Menus { get; set; }
    }
}