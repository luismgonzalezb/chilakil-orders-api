using System.ComponentModel.DataAnnotations;

namespace chilakil.orders.business.Dtos
{
    public class TokenRequest
    {
        [Required] public string userKey { get; set; }

        [Required] public string apiKey { get; set; }
    }
}