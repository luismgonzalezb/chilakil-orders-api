﻿using System.Collections.Generic;

namespace chilakil.orders.business.Dtos
{
    public class MenuItemDto
    {
        public int MenuItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }

        public ICollection<MenuIngredientsDto> Ingredients { get; set; }
    }
}