﻿namespace chilakil.orders.business.Dtos
{
    public class LocationConfigDto
    {
        public int LocationConfigId { get; set; }
        public sbyte Pickup { get; set; }
        public sbyte Delivery { get; set; }
        public sbyte? OrderAhead { get; set; }

        public LocationsDto Location { get; set; }
    }
}