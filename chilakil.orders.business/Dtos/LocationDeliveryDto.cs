﻿namespace chilakil.orders.business.Dtos
{
    public enum ZoneTypes
    {
        ZIP = 'Z',
        AREA = 'A',
        RADIUS = 'R'
    }

    public enum PriceTypes
    {
        PERCENT = '%',
        PRICE = '$'
    }

    public class LocationDeliveryDto
    {
        public long LocationDeliveryId { get; set; }
        public string ZoneValue { get; set; }
        public ZoneTypes ZoneType { get; set; }
        public double? PriceValue { get; set; }
        public PriceTypes PriceType { get; set; }

        public LocationsDto Location { get; set; }
    }
}