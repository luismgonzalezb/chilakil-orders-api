using System.Linq;
using AutoMapper;
using chilakil.orders.business.Dtos;
using chilakil.orders.infrastructure.Data.Models;

namespace chilakil.orders.business.Middleware
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Users, UsersDto>()
                .ForMember(
                    destination => destination.Restaurants,
                    options => options.MapFrom(
                        src => src.RestaurantUsers.Select(_src => _src.Restaurant).ToList()));
            CreateMap<UsersDto, Users>()
                .ForMember(d => d.UserId, o => o.Ignore())
                .ForMember(d => d.AccountKitId, o => o.Ignore())
                .ForMember(d => d.Phone, o => o.Ignore())
                .ForMember(d => d.CreatedAt, o => o.Ignore())
                .ForMember(d => d.UpdatedAt, o => o.Ignore())
                .ForMember(d => d.RoleId, o => o.Ignore())
                .ForMember(d => d.RestaurantUsers, o => o.Ignore());

            CreateMap<Users, UsersSecurityDto>()
                .ForMember(
                    destination => destination.Restaurants,
                    options => options.MapFrom(
                        src => src.RestaurantUsers.Select(_src => _src.Restaurant).ToList()));
            CreateMap<UsersSecurityDto, Users>()
                .ForMember(d => d.CreatedAt, o => o.Ignore())
                .ForMember(d => d.UpdatedAt, o => o.Ignore())
                .ForMember(d => d.RoleId, o => o.Ignore())
                .ForMember(d => d.RestaurantUsers, o => o.Ignore());

            CreateMap<Roles, RolesDto>();
            CreateMap<RolesDto, Roles>()
                .ForMember(d => d.Users, o => o.Ignore());

            CreateMap<Restaurant, RestaurantDto>()
                .ForMember(
                    destination => destination.Users,
                    options => options.MapFrom(
                        src => src.RestaurantUsers.Select(_src => _src.User).ToList()));
            CreateMap<RestaurantDto, Restaurant>()
                .ForMember(d => d.CreatedAt, o => o.Ignore())
                .ForMember(d => d.UpdatedAt, o => o.Ignore())
                .ForMember(d => d.RestaurantUsers, o => o.Ignore());

            CreateMap<Locations, LocationsDto>()
                .ForMember(
                    destination => destination.Menus,
                    options => options.MapFrom(
                        src => src.LocationMenus.Select(_src => _src.Menu).ToList()));
            CreateMap<LocationsDto, Locations>()
                .ForMember(d => d.RestaurantId, o => o.Ignore())
                .ForMember(d => d.CreatedAt, o => o.Ignore())
                .ForMember(d => d.UpdatedAt, o => o.Ignore())
                .ForMember(d => d.LocationMenus, o => o.Ignore());

            CreateMap<LocationConfig, LocationConfigDto>();
            CreateMap<LocationConfigDto, LocationConfig>()
                .ForMember(d => d.LocationId, o => o.Ignore());

            CreateMap<LocationDelivery, LocationDeliveryDto>();
            CreateMap<LocationDeliveryDto, LocationDelivery>()
                .ForMember(d => d.LocationId, o => o.Ignore());

            CreateMap<Menus, MenusDto>()
                .ForMember(
                    destination => destination.Locations,
                    options => options.MapFrom(
                        src => src.LocationMenus.Select(_src => _src.Location).ToList()));
            CreateMap<MenusDto, Menus>()
                .ForMember(d => d.RestaurantId, o => o.Ignore())
                .ForMember(d => d.CreatedAt, o => o.Ignore())
                .ForMember(d => d.UpdatedAt, o => o.Ignore())
                .ForMember(d => d.LocationMenus, o => o.Ignore());

            CreateMap<MenuItem, MenuItemDto>()
                .ForMember(
                    dest => dest.Ingredients,
                    opt => opt.MapFrom(
                        src => src.MenuItemIngredients.Select(_src => _src.MenuIngredient).ToList()));
            CreateMap<MenuItemDto, MenuItem>()
                .ForMember(d => d.CreatedAt, o => o.Ignore())
                .ForMember(d => d.UpdatedAt, o => o.Ignore())
                .ForMember(d => d.MenuCategoryItems, o => o.Ignore())
                .ForMember(d => d.MenuItemIngredients, o => o.Ignore())
                .ForMember(d => d.MenuItems, o => o.Ignore());

            CreateMap<MenuCategory, MenuCategoryDto>();
            CreateMap<MenuCategoryDto, MenuCategory>()
                .ForMember(d => d.CreatedAt, o => o.Ignore())
                .ForMember(d => d.UpdatedAt, o => o.Ignore())
                .ForMember(d => d.MenuCategories, o => o.Ignore());

            CreateMap<MenuServingTimes, MenuServingTimesDto>();
            CreateMap<MenuServingTimesDto, MenuServingTimes>();

            CreateMap<MenuIngredients, MenuIngredientsDto>();
            CreateMap<MenuIngredientsDto, MenuIngredients>()
                .ForMember(d => d.CreatedAt, o => o.Ignore())
                .ForMember(d => d.UpdatedAt, o => o.Ignore())
                .ForMember(d => d.MenuItemIngredients, o => o.Ignore());
        }
    }
}