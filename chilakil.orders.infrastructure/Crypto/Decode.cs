using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace chilakil.orders.infrastructure.Crypto
{
    public static class Decode
    {
        public static string EncryptString(string plainText, string key)
        {
            byte[] encrypted;
            byte[] iv;

            using (var aesAlg = Aes.Create())
            {
                aesAlg.Key = Encoding.ASCII.GetBytes(key);

                aesAlg.GenerateIV();
                iv = aesAlg.IV;

                aesAlg.Mode = CipherMode.CBC;

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            var combinedIvCt = new byte[iv.Length + encrypted.Length];
            Array.Copy(iv, 0, combinedIvCt, 0, iv.Length);
            Array.Copy(encrypted, 0, combinedIvCt, iv.Length, encrypted.Length);

            // Return the encrypted bytes from the memory stream.
            var final = new StringBuilder();
            final.Append(Convert.ToBase64String(iv));
            final.Append(":");
            final.Append(Convert.ToBase64String(encrypted));
            return final.ToString();
        }

        public static string DecryptStringFromBytes(string encryptedText, string key)
        {
            var _key = Encoding.ASCII.GetBytes(key);
            var enctiptionParts = encryptedText.Split(":");
            var cipherText = Convert.FromBase64String(enctiptionParts[1]);
            var iv = Convert.FromBase64String(enctiptionParts[0]);
            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an Aes object 
            // with the specified key and IV. 
            using (var aesAlg = Aes.Create())
            {
                aesAlg.Key = _key;
                aesAlg.IV = iv;
                aesAlg.Mode = CipherMode.CBC;

                // Create a decrytor to perform the stream transform.
                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }
    }
}