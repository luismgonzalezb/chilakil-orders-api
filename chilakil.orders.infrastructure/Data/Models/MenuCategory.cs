﻿using System;
using System.Collections.Generic;

namespace chilakil.orders.infrastructure.Data.Models
{
    public class MenuCategory
    {
        public MenuCategory()
        {
            MenuCategories = new HashSet<MenuCategories>();
        }

        public int MenuCategoryId { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public ICollection<MenuCategories> MenuCategories { get; set; }
    }
}