﻿using System;
using System.Collections.Generic;

namespace chilakil.orders.infrastructure.Data.Models
{
    public class MenuIngredients
    {
        public MenuIngredients()
        {
            MenuItemIngredients = new HashSet<MenuItemIngredients>();
        }

        public int MenuIngredientId { get; set; }
        public string Name { get; set; }
        public int? Alergenic { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public ICollection<MenuItemIngredients> MenuItemIngredients { get; set; }
    }
}