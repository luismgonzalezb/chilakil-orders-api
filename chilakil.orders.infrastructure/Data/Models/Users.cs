﻿using System;
using System.Collections.Generic;

namespace chilakil.orders.infrastructure.Data.Models
{
    public class Users
    {
        public Users()
        {
            RestaurantUsers = new HashSet<RestaurantUsers>();
        }

        public int UserId { get; set; }
        public string AccountKitId { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int RoleId { get; set; }

        public Roles Role { get; set; }
        public ICollection<RestaurantUsers> RestaurantUsers { get; set; }
    }
}