﻿using System;
using System.Collections.Generic;

namespace chilakil.orders.infrastructure.Data.Models
{
    public class MenuItem
    {
        public MenuItem()
        {
            MenuCategoryItems = new HashSet<MenuCategoryItems>();
            MenuItemIngredients = new HashSet<MenuItemIngredients>();
            MenuItems = new HashSet<MenuItems>();
        }

        public int MenuItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public ICollection<MenuCategoryItems> MenuCategoryItems { get; set; }
        public ICollection<MenuItemIngredients> MenuItemIngredients { get; set; }
        public ICollection<MenuItems> MenuItems { get; set; }
    }
}