﻿using System;
using System.Collections.Generic;

namespace chilakil.orders.infrastructure.Data.Models
{
    public class Locations
    {
        public Locations()
        {
            LocationConfig = new HashSet<LocationConfig>();
            LocationDelivery = new HashSet<LocationDelivery>();
            LocationMenus = new HashSet<LocationMenus>();
        }

        public int LocationId { get; set; }
        public int? RestaurantId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public Restaurant Restaurant { get; set; }
        public ICollection<LocationConfig> LocationConfig { get; set; }
        public ICollection<LocationDelivery> LocationDelivery { get; set; }
        public ICollection<LocationMenus> LocationMenus { get; set; }
    }
}