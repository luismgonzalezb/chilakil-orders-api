﻿using System;
using System.Collections.Generic;

namespace chilakil.orders.infrastructure.Data.Models
{
    public class Restaurant
    {
        public Restaurant()
        {
            Locations = new HashSet<Locations>();
            RestaurantUsers = new HashSet<RestaurantUsers>();
        }

        public int RestaurantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; }

        public ICollection<Locations> Locations { get; set; }
        public ICollection<RestaurantUsers> RestaurantUsers { get; set; }
    }
}