﻿using Microsoft.EntityFrameworkCore;

namespace chilakil.orders.infrastructure.Data.Models
{
    public class ChilakilContext : DbContext
    {
        public ChilakilContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<LocationConfig> LocationConfig { get; set; }
        public DbSet<LocationDelivery> LocationDelivery { get; set; }
        public DbSet<LocationMenus> LocationMenus { get; set; }
        public DbSet<Locations> Locations { get; set; }
        public DbSet<MenuCategories> MenuCategories { get; set; }
        public DbSet<MenuCategory> MenuCategory { get; set; }
        public DbSet<MenuCategoryItems> MenuCategoryItems { get; set; }
        public DbSet<MenuIngredients> MenuIngredients { get; set; }
        public DbSet<MenuItem> MenuItem { get; set; }
        public DbSet<MenuItemIngredients> MenuItemIngredients { get; set; }
        public DbSet<MenuItems> MenuItems { get; set; }
        public DbSet<MenuServingTimes> MenuServingTimes { get; set; }
        public DbSet<Menus> Menus { get; set; }
        public DbSet<Restaurant> Restaurant { get; set; }
        public DbSet<RestaurantUsers> RestaurantUsers { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LocationConfig>(entity =>
            {
                entity.HasIndex(e => e.LocationId)
                    .HasName("LocationConfig_Locations_locationId_fk");

                entity.Property(e => e.LocationConfigId)
                    .HasColumnName("locationConfigId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Delivery)
                    .HasColumnName("delivery")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.LocationId)
                    .HasColumnName("locationId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OrderAhead)
                    .HasColumnName("orderAhead")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Pickup)
                    .HasColumnName("pickup")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.LocationConfig)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("LocationConfig_Locations_locationId_fk");
            });

            modelBuilder.Entity<LocationDelivery>(entity =>
            {
                entity.HasKey(e => e.LocationDeliveryId)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.LocationId)
                    .HasName("LocationDelivery_Locations_locationId_fk");

                entity.Property(e => e.LocationDeliveryId)
                    .HasColumnName("locationDelivery")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.LocationId)
                    .HasColumnName("locationId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PriceValue).HasColumnName("priceValue");

                entity.Property(e => e.ZoneValue)
                    .HasColumnName("zoneValue")
                    .HasColumnType("text");

                entity.Property(e => e.ZoneType)
                    .HasColumnName("zoneType")
                    .HasColumnType("enum('Z','A','R')");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.LocationDelivery)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("LocationDelivery_Locations_locationId_fk");
            });

            modelBuilder.Entity<LocationMenus>(entity =>
            {
                entity.HasKey(e => e.LocationMenuId)
                    .HasName("PRIMARY");

                entity.ToTable("Location_Menus");

                entity.HasIndex(e => e.LocationId)
                    .HasName("Location_Menus_Location_locationId_fk");

                entity.HasIndex(e => e.MenuId)
                    .HasName("Location_Menus_Menus_menuId_fk");

                entity.Property(e => e.LocationMenuId)
                    .HasColumnName("location_menu_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LocationId)
                    .HasColumnName("locationId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MenuId)
                    .HasColumnName("menuId")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.LocationMenus)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("Location_Menus_Location_locationId_fk");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.LocationMenus)
                    .HasForeignKey(d => d.MenuId)
                    .HasConstraintName("Location_Menus_Menus_menuId_fk");
            });

            modelBuilder.Entity<Locations>(entity =>
            {
                entity.HasKey(e => e.LocationId)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.LocationId)
                    .HasName("Location_locationId_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.RestaurantId)
                    .HasName("Location_Restaurant_restaurantId_fk");

                entity.Property(e => e.LocationId)
                    .HasColumnName("locationId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Address1)
                    .HasColumnName("address1")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.Address2)
                    .HasColumnName("address2")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("createdAt")
                    .HasColumnType("datetime");

                entity.Property(e => e.Lat)
                    .HasColumnName("lat")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Long)
                    .HasColumnName("long")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.RestaurantId)
                    .HasColumnName("restaurantId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updatedAt")
                    .HasColumnType("datetime");

                entity.Property(e => e.Zipcode)
                    .HasColumnName("zipcode")
                    .HasColumnType("varchar(20)");

                entity.HasOne(d => d.Restaurant)
                    .WithMany(p => p.Locations)
                    .HasForeignKey(d => d.RestaurantId)
                    .HasConstraintName("Location_Restaurant_restaurantId_fk");
            });

            modelBuilder.Entity<MenuCategories>(entity =>
            {
                entity.ToTable("Menu_Categories");

                entity.HasIndex(e => e.MenuCategoryId)
                    .HasName("Menu_Category_MenuCategory_menuCategoryId_fk");

                entity.HasIndex(e => e.MenuId)
                    .HasName("Menu_Category_Menus_menuId_fk");

                entity.Property(e => e.MenuCategoriesId)
                    .HasColumnName("menu_categories_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MenuCategoryId)
                    .HasColumnName("menuCategoryId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MenuId)
                    .HasColumnName("menuId")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.MenuCategoryIdNavigation)
                    .WithMany(p => p.MenuCategories)
                    .HasForeignKey(d => d.MenuCategoryId)
                    .HasConstraintName("Menu_Category_MenuCategory_menuCategoryId_fk");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.MenuCategories)
                    .HasForeignKey(d => d.MenuId)
                    .HasConstraintName("Menu_Category_Menus_menuId_fk");
            });

            modelBuilder.Entity<MenuCategory>(entity =>
            {
                entity.HasKey(e => e.MenuCategoryId)
                    .HasName("PRIMARY");

                entity.ToTable("MenuCategory");

                entity.Property(e => e.MenuCategoryId)
                    .HasColumnName("menuCategoryId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("createdAt")
                    .HasColumnType("timestamp");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updatedAt")
                    .HasColumnType("timestamp");
            });

            modelBuilder.Entity<MenuCategoryItems>(entity =>
            {
                entity.HasKey(e => e.MenuCategoryItemId)
                    .HasName("PRIMARY");

                entity.ToTable("Menu_Category_Items");

                entity.HasIndex(e => e.MenuCategoryId)
                    .HasName("Menu_Category_Items_Menu_Category_menu_category_id_fk");

                entity.HasIndex(e => e.MenuItemId)
                    .HasName("Menu_Category_Items_MenuItem_menuItemId_fk");

                entity.Property(e => e.MenuCategoryItemId)
                    .HasColumnName("menu_category_item_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.MenuCategoryId)
                    .HasColumnName("menu_category_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MenuItemId)
                    .HasColumnName("menuItemId")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.MenuCategories)
                    .WithMany(p => p.MenuCategoryItems)
                    .HasForeignKey(d => d.MenuCategoryId)
                    .HasConstraintName("Menu_Category_Items_Menu_Category_menu_category_id_fk");

                entity.HasOne(d => d.MenuItem)
                    .WithMany(p => p.MenuCategoryItems)
                    .HasForeignKey(d => d.MenuItemId)
                    .HasConstraintName("Menu_Category_Items_MenuItem_menuItemId_fk");
            });

            modelBuilder.Entity<MenuIngredients>(entity =>
            {
                entity.HasKey(e => e.MenuIngredientId)
                    .HasName("PRIMARY");

                entity.Property(e => e.MenuIngredientId)
                    .HasColumnName("menuIngredientId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Alergenic)
                    .HasColumnName("alergenic")
                    .HasColumnType("int(1)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("createdAt")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updatedAt")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<MenuItem>(entity =>
            {
                entity.Property(e => e.MenuItemId)
                    .HasColumnName("menuItemId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("createdAt")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,0)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updatedAt")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<MenuItemIngredients>(entity =>
            {
                entity.ToTable("MenuItem_Ingredients");

                entity.HasIndex(e => e.MenuIngredientId)
                    .HasName("MenuItem_Ingredients_MenuIngredients_menuIngredientId_fk");

                entity.HasIndex(e => e.MenuItemId)
                    .HasName("MenuIngredients_MenuItem_menuItemId_fk");

                entity.Property(e => e.MenuItemIngredientsId)
                    .HasColumnName("menuItem_Ingredients_Id")
                    .HasColumnType("bigint(11)");

                entity.Property(e => e.MenuIngredientId)
                    .HasColumnName("menuIngredientId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MenuItemId)
                    .HasColumnName("menuItemId")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.MenuIngredient)
                    .WithMany(p => p.MenuItemIngredients)
                    .HasForeignKey(d => d.MenuIngredientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MenuItem_Ingredients_MenuIngredients_menuIngredientId_fk");

                entity.HasOne(d => d.MenuItem)
                    .WithMany(p => p.MenuItemIngredients)
                    .HasForeignKey(d => d.MenuItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("MenuIngredients_MenuItem_menuItemId_fk");
            });

            modelBuilder.Entity<MenuItems>(entity =>
            {
                entity.ToTable("Menu_Items");

                entity.HasIndex(e => e.MenuId)
                    .HasName("Menu_Items_Menus_menuId_fk");

                entity.HasIndex(e => e.MenuItemId)
                    .HasName("Menu_Items_MenuItem_menuItemId_fk");

                entity.Property(e => e.MenuItemsId)
                    .HasColumnName("menu_items_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.MenuId)
                    .HasColumnName("menuId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MenuItemId)
                    .HasColumnName("menuItemId")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.MenuItems)
                    .HasForeignKey(d => d.MenuId)
                    .HasConstraintName("Menu_Items_Menus_menuId_fk");

                entity.HasOne(d => d.MenuItem)
                    .WithMany(p => p.MenuItems)
                    .HasForeignKey(d => d.MenuItemId)
                    .HasConstraintName("Menu_Items_MenuItem_menuItemId_fk");
            });

            modelBuilder.Entity<MenuServingTimes>(entity =>
            {
                entity.HasIndex(e => e.MenuId)
                    .HasName("MenuServingTimes_Menus_menuId_fk");

                entity.Property(e => e.MenuServingTimesId)
                    .HasColumnName("menuServingTimesId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.EndTime)
                    .HasColumnName("endTime")
                    .HasColumnType("timestamp");

                entity.Property(e => e.Day)
                    .HasColumnName("day")
                    .HasColumnType("enum('M', 'T', 'W', 'R', 'F', 'S', 'U')");

                entity.Property(e => e.MenuId)
                    .HasColumnName("menuId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StartTime)
                    .HasColumnName("startTime")
                    .HasColumnType("timestamp");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.MenuServingTimes)
                    .HasForeignKey(d => d.MenuId)
                    .HasConstraintName("MenuServingTimes_Menus_menuId_fk");
            });

            modelBuilder.Entity<Menus>(entity =>
            {
                entity.HasKey(e => e.MenuId)
                    .HasName("PRIMARY");

                entity.Property(e => e.MenuId)
                    .HasColumnName("menuId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("createdAt")
                    .HasColumnType("timestamp");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(254)");

                entity.Property(e => e.RestaurantId)
                    .HasColumnName("restaurantId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updatedAt")
                    .HasColumnType("timestamp");
            });

            modelBuilder.Entity<Restaurant>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("Restaurant_Name_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.RestaurantId)
                    .HasName("Restaurant_restaurantId_uindex")
                    .IsUnique();

                entity.Property(e => e.RestaurantId)
                    .HasColumnName("restaurantId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("createdAt")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updatedAt")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<RestaurantUsers>(entity =>
            {
                entity.HasKey(e => e.RestaurantUserId)
                    .HasName("PRIMARY");

                entity.ToTable("Restaurant_Users");

                entity.HasIndex(e => e.RestaurantId)
                    .HasName("Restaurant_Users_Restaurant_restaurantId_fk");

                entity.HasIndex(e => e.UserId)
                    .HasName("Restaurant_Users_users_userId_fk");

                entity.Property(e => e.RestaurantUserId)
                    .HasColumnName("restaurant_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RestaurantId)
                    .HasColumnName("restaurantId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Restaurant)
                    .WithMany(p => p.RestaurantUsers)
                    .HasForeignKey(d => d.RestaurantId)
                    .HasConstraintName("Restaurant_Users_Restaurant_restaurantId_fk");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.RestaurantUsers)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("Restaurant_Users_users_userId_fk");
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.HasKey(e => e.RoleId)
                    .HasName("PRIMARY");

                entity.ToTable("roles");

                entity.HasIndex(e => e.RoleId)
                    .HasName("roles_id_uindex")
                    .IsUnique();

                entity.Property(e => e.RoleId)
                    .HasColumnName("roleId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("int(1)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasColumnName("role")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PRIMARY");

                entity.ToTable("users");

                entity.HasIndex(e => e.AccountKitId)
                    .HasName("account_kit_id")
                    .IsUnique();

                entity.HasIndex(e => e.RoleId)
                    .HasName("users_roles_id_fk");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AccountKitId)
                    .IsRequired()
                    .HasColumnName("account_kit_id")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("createdAt")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Lastname)
                    .HasColumnName("lastname")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.RoleId)
                    .HasColumnName("roleId")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updatedAt")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("users_roles_id_fk");
            });
        }
    }
}