﻿using System;

namespace chilakil.orders.infrastructure.Data.Models
{
    public enum Days
    {
        M = 'M',
        T = 'T',
        W = 'W',
        R = 'R',
        F = 'F',
        S = 'S',
        U = 'U'
    }

    public class MenuServingTimes
    {
        public long MenuServingTimesId { get; set; }
        public int? MenuId { get; set; }
        public Days Day { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public Menus Menu { get; set; }
    }
}