﻿using System.Collections.Generic;

namespace chilakil.orders.infrastructure.Data.Models
{
    public class MenuCategories
    {
        public MenuCategories()
        {
            MenuCategoryItems = new HashSet<MenuCategoryItems>();
        }

        public int MenuCategoriesId { get; set; }
        public int? MenuId { get; set; }
        public int? MenuCategoryId { get; set; }

        public Menus Menu { get; set; }
        public MenuCategory MenuCategoryIdNavigation { get; set; }
        public ICollection<MenuCategoryItems> MenuCategoryItems { get; set; }
    }
}