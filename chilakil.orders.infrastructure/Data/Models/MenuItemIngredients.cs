﻿namespace chilakil.orders.infrastructure.Data.Models
{
    public class MenuItemIngredients
    {
        public long MenuItemIngredientsId { get; set; }
        public int MenuIngredientId { get; set; }
        public int MenuItemId { get; set; }

        public MenuIngredients MenuIngredient { get; set; }
        public MenuItem MenuItem { get; set; }
    }
}