﻿namespace chilakil.orders.infrastructure.Data.Models
{
    public class LocationConfig
    {
        public int LocationConfigId { get; set; }
        public int? LocationId { get; set; }
        public sbyte Pickup { get; set; }
        public sbyte Delivery { get; set; }
        public sbyte? OrderAhead { get; set; }

        public Locations Location { get; set; }
    }
}