﻿using System;
using System.Collections.Generic;

namespace chilakil.orders.infrastructure.Data.Models
{
    public class Menus
    {
        public Menus()
        {
            LocationMenus = new HashSet<LocationMenus>();
            MenuCategories = new HashSet<MenuCategories>();
            MenuItems = new HashSet<MenuItems>();
            MenuServingTimes = new HashSet<MenuServingTimes>();
        }

        public int MenuId { get; set; }
        public int? RestaurantId { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public ICollection<LocationMenus> LocationMenus { get; set; }
        public ICollection<MenuCategories> MenuCategories { get; set; }
        public ICollection<MenuItems> MenuItems { get; set; }
        public ICollection<MenuServingTimes> MenuServingTimes { get; set; }
    }
}