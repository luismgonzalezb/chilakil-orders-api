﻿namespace chilakil.orders.infrastructure.Data.Models
{
    public class RestaurantUsers
    {
        public int RestaurantUserId { get; set; }
        public int? RestaurantId { get; set; }
        public int? UserId { get; set; }

        public Restaurant Restaurant { get; set; }
        public Users User { get; set; }
    }
}