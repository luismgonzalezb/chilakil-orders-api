﻿namespace chilakil.orders.infrastructure.Data.Models
{
    public class LocationMenus
    {
        public int LocationMenuId { get; set; }
        public int? LocationId { get; set; }
        public int? MenuId { get; set; }

        public Locations Location { get; set; }
        public Menus Menu { get; set; }
    }
}