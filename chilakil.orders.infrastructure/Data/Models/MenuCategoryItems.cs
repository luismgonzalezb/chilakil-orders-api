﻿namespace chilakil.orders.infrastructure.Data.Models
{
    public class MenuCategoryItems
    {
        public long MenuCategoryItemId { get; set; }
        public int? MenuCategoryId { get; set; }
        public int? MenuItemId { get; set; }

        public MenuCategories MenuCategories { get; set; }
        public MenuItem MenuItem { get; set; }
    }
}