﻿namespace chilakil.orders.infrastructure.Data.Models
{
    public enum ZoneTypes
    {
        Zip = 'Z',
        Area = 'A',
        Radius = 'R'
    }

    public enum PriceTypes
    {
        Percent = '%',
        Price = '$'
    }

    public class LocationDelivery
    {
        public long LocationDeliveryId { get; set; }
        public int? LocationId { get; set; }
        public string ZoneValue { get; set; }
        public ZoneTypes ZoneType { get; set; }
        public double? PriceValue { get; set; }
        public PriceTypes PriceType { get; set; }

        public Locations Location { get; set; }
    }
}