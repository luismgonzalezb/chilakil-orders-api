﻿namespace chilakil.orders.infrastructure.Data.Models
{
    public class MenuItems
    {
        public long MenuItemsId { get; set; }
        public int? MenuId { get; set; }
        public int? MenuItemId { get; set; }

        public Menus Menu { get; set; }
        public MenuItem MenuItem { get; set; }
    }
}