﻿using System.Collections.Generic;

namespace chilakil.orders.infrastructure.Data.Models
{
    public class Roles
    {
        public Roles()
        {
            Users = new HashSet<Users>();
        }

        public int RoleId { get; set; }
        public string Role { get; set; }
        public int Active { get; set; }

        public ICollection<Users> Users { get; set; }
    }
}