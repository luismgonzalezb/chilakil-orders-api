﻿using System.Collections.Generic;
using chilakil.orders.infrastructure.Data.Models;

namespace chilakil.orders.infrastructure.Data.Contracts
{
    public interface IRestaurantRepository
    {
        Restaurant GetRestaurant(int restaurantId);

        Restaurant AddRestaurant(string name, string description, int userId);

        IEnumerable<Restaurant> GetUserRestaurants(int userId);

        bool DeleteRestaurant(int userId, int restaurantId);
    }
}