﻿using System.Collections.Generic;
using chilakil.orders.infrastructure.Data.Models;

namespace chilakil.orders.infrastructure.Data.Contracts
{
    public interface IMenuItemRepository
    {
        IEnumerable<MenuItem> GetAll();

        MenuItem Add(MenuItem menuItem);
    }
}