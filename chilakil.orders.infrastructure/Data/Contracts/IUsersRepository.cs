﻿using chilakil.orders.infrastructure.Data.Models;

namespace chilakil.orders.infrastructure.Data.Contracts
{
    public interface IUsersRepository
    {
        Users Update(int id, string accountKitId, string phone, Users user);

        Users GetUserOrCreateByAccountId(string accountKitId, string phone);

        Users GetUserByAuth(int id, string accountKitId, string phone);
    }
}