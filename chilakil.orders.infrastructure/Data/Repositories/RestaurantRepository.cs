﻿using System;
using System.Collections.Generic;
using System.Linq;
using chilakil.orders.infrastructure.Data.Contracts;
using chilakil.orders.infrastructure.Data.Models;

namespace chilakil.orders.infrastructure.Data.Repositories
{
    public class RestaurantRepository : IRestaurantRepository
    {
        private readonly ChilakilContext _context;

        public RestaurantRepository(ChilakilContext context)
        {
            _context = context;
        }

        public Restaurant GetRestaurant(int restaurantId)
        {
            var restaurant = _context.Restaurant.SingleOrDefault(res => res.RestaurantId == restaurantId);
            return restaurant;
        }

        public Restaurant AddRestaurant(string name, string description, int userId)
        {
            var restaurant = new Restaurant
            {
                Name = name,
                Description = description,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
            _context.Restaurant.Add(restaurant);
            _context.SaveChanges();
            var restaurantUser = new RestaurantUsers
            {
                RestaurantId = restaurant.RestaurantId,
                UserId = userId
            };
            _context.RestaurantUsers.Add(restaurantUser);
            _context.SaveChanges();
            return restaurant;
        }

        public IEnumerable<Restaurant> GetUserRestaurants(int userId)
        {
            var restaurants = _context.Restaurant.Where(r => r.RestaurantUsers.Any(ru => ru.UserId == userId)).ToList();
            return restaurants;
        }

        public bool DeleteRestaurant(int userId, int restaurantId)
        {
            var restaurant = _context.Restaurant.SingleOrDefault(r =>
                r.RestaurantId == restaurantId && r.RestaurantUsers.Any(ru => ru.UserId == userId));
            if (restaurant == null) return false;
            _context.Restaurant.Remove(restaurant);
            _context.SaveChanges();
            return true;
        }
    }
}