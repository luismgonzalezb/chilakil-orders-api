﻿using System.Collections.Generic;
using System.Linq;
using chilakil.orders.infrastructure.Data.Contracts;
using chilakil.orders.infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace chilakil.orders.infrastructure.Data.Repositories
{
    public class MenuItemRepository : IMenuItemRepository
    {
        private readonly ChilakilContext _context;

        public MenuItemRepository(ChilakilContext context)
        {
            _context = context;
        }

        public IEnumerable<MenuItem> GetAll()
        {
            return _context.MenuItem
                .Include(e => e.MenuItemIngredients)
                .ThenInclude(e => e.MenuIngredient)
                .ToList();
        }

        public MenuItem Add(MenuItem menuItem)
        {
            _context.MenuItem.AddAsync(menuItem);
            _context.SaveChangesAsync();
            return menuItem;
        }
    }
}