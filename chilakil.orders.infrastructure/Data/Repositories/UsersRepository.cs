﻿using System;
using System.Linq;
using chilakil.orders.infrastructure.Data.Contracts;
using chilakil.orders.infrastructure.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace chilakil.orders.infrastructure.Data.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly ChilakilContext _context;

        public UsersRepository(ChilakilContext context)
        {
            _context = context;
        }

        public Users Update(int id, string accountKitId, string phone, Users user)
        {
            var _user = GetUserByAuth(id, accountKitId, phone);
            if (_user == null) return null;
            _user.UpdatedAt = DateTime.Now;
            _user.Name = user.Name;
            _user.Lastname = user.Lastname;
            _user.Email = user.Email;
            _context.SaveChanges();
            return _user;
        }

        public Users GetUserOrCreateByAccountId(string accountKitId, string phone)
        {
            var currentUser = _context.Users.SingleOrDefault(u => u.AccountKitId == accountKitId);
            if (currentUser != null) return currentUser;
            var newUser = new Users
            {
                AccountKitId = accountKitId,
                Phone = phone,
                RoleId = 1
            };
            AddUser(newUser);
            return newUser;
        }

        public Users GetUserByAuth(int id, string accountKitId, string phone)
        {
            var user = _context.Users.Include(r => r.Role)
                .SingleOrDefault(u => u.UserId == id && u.AccountKitId == accountKitId && u.Phone == phone);
            return user;
        }

        private void AddUser(Users user)
        {
            user.CreatedAt = DateTime.Now;
            user.UpdatedAt = DateTime.Now;
            _context.Users.Add(user);
            _context.SaveChanges();
        }
    }
}