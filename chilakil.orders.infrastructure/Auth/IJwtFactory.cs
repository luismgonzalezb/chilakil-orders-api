namespace chilakil.orders.infrastructure.Auth
{
    public interface IJwtFactory
    {
        string GenerateEncodedToken(string userKey);
    }
}