﻿namespace chilakil.orders.infrastructure.Auth
{
    public class JwtIssuerOptions : IJwtIssuerOptions
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }
    }
}