﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using chilakil.orders.infrastructure.Crypto;
using chilakil.orders.infrastructure.Data.Contracts;
using chilakil.orders.infrastructure.Data.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace chilakil.orders.infrastructure.Auth
{
    public class JwtFactory : IJwtFactory
    {
        private readonly IJwtIssuerOptions _jwtIssuerOptions;
        private readonly IUsersRepository _repository;

        public JwtFactory(IUsersRepository repository, IConfiguration configuration)
        {
            _jwtIssuerOptions = new JwtIssuerOptions();
            configuration.Bind("JwtIssuerOptions", _jwtIssuerOptions);
            _repository = repository;
        }

        public string GenerateEncodedToken(string userKey)
        {
            var user = GetUserByEncryptedKey(userKey);
            if (user == null) return null;

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new Claim(ClaimTypes.HomePhone, user.Phone),
                new Claim(ClaimTypes.UserData, user.AccountKitId),
                new Claim(ClaimTypes.Role, user.Role.Role)
            };

            var credentials = GetCredentials();
            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                audience: _jwtIssuerOptions.Audience,
                issuer: _jwtIssuerOptions.Issuer,
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private Users GetUserByEncryptedKey(string userKey)
        {
            var apiKey = Environment.GetEnvironmentVariable("CHILAKIL_API_KEY");
            var decodedUserInfo = Decode.DecryptStringFromBytes(userKey, apiKey);
            var userInfoParts = decodedUserInfo.Split(":");

            var id = int.Parse(userInfoParts[0]);
            var accountKitId = userInfoParts[1];
            var phone = userInfoParts[2];
            var user = _repository.GetUserByAuth(id, accountKitId, phone);
            return user;
        }

        private static SigningCredentials GetCredentials()
        {
            var tokenKey = Environment.GetEnvironmentVariable("CHILAKIL_COOKIE_KEY");
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenKey));
            return new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        }
    }
}