﻿namespace chilakil.orders.infrastructure.Auth
{
    public interface IJwtIssuerOptions
    {
        string Issuer { get; set; }

        string Audience { get; set; }
    }
}